const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const listItemScheme = require('./ListItem');

var userScheme = mongoose.Schema({
    name: { type: String, required: true, trim: true},
    email: { type: String, unique: true, required: true, trim: true},
    password: { type: String, require: true, trim: true, ref: 'password'},
    createdAt: { type: Date, default: new Date, required: true },
    updatedAt: { type: Date, default: new Date, required: true },
    list: [listItemScheme]
});


userScheme.pre('save', function (next) {
    this.list.sort((item1, item2)=>{
        if(item1.updatedAt > item2.updatedAt){
            return 1;
        }
        return -1;
    });
    next();
});

var User = mongoose.model('User', userScheme);

module.exports = User;