const mongoose = require('mongoose');

var listItemScheme = mongoose.Schema({
    createdAt: { type: Date, default: new Date, required: true},
    updatedAt: { type: Date, default: new Date, required: true },
    title: String,
    body: String,
    tags: [String],
    author: { type: String, required: true},
    complete: {type: Boolean, default: false}
});

listItemScheme.method('update', function(updates, cb){
    Object.assign(this, updates, {updatedAt: new Date});
    this.parent().save(cb);
});

// var ListItem = mongoose.model('ListItem', listItemScheme);

module.exports = listItemScheme;