var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
const config = require('../config');

function tagParser(text){
    if(!text) return [];
    var textArray = text.split(' ');
    var tagList = [];
    textArray.forEach((item) => {
        if (item[0] === "#") {
            if (item[item.length - 1] === ".") {
                item = item.slice(0, item.length - 1);
            }
            tagList.push(item.slice(1));
        }
    })
    return tagList;
}

function validateToken(req, res, next){
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if (token) {
        jwt.verify(token, config.secret, function (err, decoded) {
            if (err) {
                return res.json({ success: false, message: 'Failed to authenticate token.' });
            } else {
                req.decoded = decoded;
                if (req.decoded.id == req.user._id) {
                    return next();
                }else{
                    return res.status(403).json({
                        success: false,
                        error: 'permission denied'
                    });
                } 
            }
        });
    }else{
        return res.status(403).json({
            success: false,
            message: 'No token provided.'
        });
    }

    
}

module.exports.tagParser = tagParser;
module.exports.validateToken = validateToken;