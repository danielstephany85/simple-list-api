var express = require('express');
var router = express.Router();
const bcrypt = require('bcrypt');
var User = require('../models/User');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
const config = require('../config');
var middleware = require('../middleware/index');


router.post('/authenticate', function(req, res, next) {
  if(req.body.email && req.body.password){

    User.findOne({ email: req.body.email })
      .lean()
      .exec(function (error, user) {
        if (error) {
          return next(error);
        }
        if (!user) {
          var err = new Error('User name does not exist');
          err.status = 401;
          return next(err);
        }
        bcrypt.compare(req.body.password, user.password, function (error, result) {
          if (result === true) {

            const payload = {
              id: user._id
            };

            var token = jwt.sign(payload, config.secret, { expiresIn: '24h' });

            var userData = {};
            for(var prop in user){
              if (prop !== 'password' || prop !== '__v'){
                userData[prop] = user[prop];
              }
            }

            res.json({
              success: true,
              data: {
                token: token,
                user: userData
              }
            });

          } else {
            return res.json({
              success: false,
              message: 'username or password is incorect',
            });
          }
        });
      });

  }else {
    var error = new Error('user email and password is required');
    error.status = 401;
    next();
  }
});

router.post('/create-account', (req, res, next) => {
  if(req.body.name && req.body.email && req.body.password){
    
    var userData = {
      "name": req.body.name,
      "email": req.body.email,
      "password": req.body.password
    };
    User.findOne({ email: req.body.email })
    .exec(function (err, user) {
      if (err) {
        return callback(err);
      } else if (user) {
        return res.json({
          success: false,
          message: `A user with email: ${req.body.email} already exists.`,
        });
      }
    });
    bcrypt.hash(userData.password, 10, (err, hash) => {
      if (err) next(err);
      userData.password = hash;
      User.create(userData, (err, user) => {
        if (err) {
          return next(err.message);
        } else {
          User.findById(user._id)
            .exec(function (err, user) {
              if (err) {
                return next(err);
              }
              const payload = {
                id: user._id
              };
              // making json web token 
              var token = jwt.sign(payload, config.secret, { expiresIn: '24h' });
              return res.json({
                success: true,
                data: {user: user, token: token}
              });
            });
        }
      });
    });
  
  }else{
    return res.json({
      success: false,
      message: 'username, email, and password are required',
    });
  }
});


module.exports = router;
