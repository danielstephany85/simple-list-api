var express = require('express');
var router = express.Router();
var User = require('../models/User');
var middleware = require('../middleware/index');
const bcrypt = require('bcrypt');

router.param('id', (req, res, next, id) => {
    User.findOne({ _id: id }, { password: false })
        .exec((err, doc) => {
            if (err){
                return res.status(403).json({
                        success: false,
                        message: err.message
                    });
            }
            if (!doc) {
                res.status(404).json({
                    success: false,
                    message: 'User not found'
                });
            }
            req.user = doc;
            return next();
        });
});

router.get('/:id', middleware.validateToken, function (req, res, next) {
    return res.json({
        success: true,
        data: {user: req.user}
    });
});


router.put('/:id', middleware.validateToken, function(req, res, next){
    if (req.body.name) {
        req.user.name = req.body.name;
    }
    if (req.body.email) {
        req.user.email = req.body.email;
    }
    if (req.body.password){
        bcrypt.hash(req.body.password, 10, (err, hash) => {
            if (err) return next(err);
            req.user.password = hash;
            updateUser(req.user)
        });
    } else {
        updateUser(req.user);
    }

    function updateUser(user){
        user.update(user, (err, result) => {
            if (err) {
                return res.status(403).send({
                    success: false,
                    message: err.message
                });
            }
            return res.json({
                success: true,
                data: result
            });
        });
    }
});

router.delete('/:id', middleware.validateToken, function(req, res, next){
    req.user.remove((err)=>{
        if (err) return next(err);
        return res.status(200).send({
            success: true,
            data: null
        });
    }) 
});

module.exports = router;