var express = require('express');
var router = express.Router();
var middleware = require('../middleware/index');
var User = require('../models/User');

router.param('userId', (req, res, next, id) => {
  User.findOne({ _id: id }, { password: false })
    .exec((err, doc) => {
      if (err) {
        return res.status(403).send({
          success: false,
          message: 'User not found'
        });
      }
      if (!doc) {
        var error = new Error('Not Found');
        error.status = 404;
        return next(error);
      }
      req.user = doc;
      return next();
    });
});

// get list of tasks
router.get('/:userId', middleware.validateToken, function (req, res, next) {
      return res.json({
        success: true,
        data: {list: req.user.list}
      });
});

// add task to list
router.post('/:userId', middleware.validateToken, function(req, res, next){
  var task = {
    author: req.body.name,
    title: req.body.title,
    body: req.body.body,
    tags: middleware.tagParser(req.body.body),
    complete: req.body.complete
  };
  if (req.body.name && req.body.title && req.body.body) {

    req.user.list.push(task);
    req.user.save((err, user) => {
      if (err) return next(err);
      return res.json({ 
        success: true,
        data: {list: user.list}
      });
    });
  } else {
    return res.status(403).json({
      success: false,
      data: {task: false}
    });
  }
});


// update single task
router.put('/:userId/:taskId', middleware.validateToken, function(req, res, next){
  var task = req.user.list.id(req.params.taskId);
  if(task){
    if (req.body.title) { task.title = req.body.title; }
    if (req.body.body) { task.body = req.body.body; }
    if (req.body.body) { task.tags = middleware.tagParser(req.body.task); }
    if (typeof (req.body.complete) === "boolean") { task.complete = req.body.complete; }
    task.update(task, function(err, result){
      if(err){
        return res.status(403).json({
          success: false,
          message: err.message
        });
      }
      return res.json({
        success: true,
        data: null
      });
    });
  }else {
    return res.status(403).json({
      success: false,
      error: 'item not found'
    });
  }
});

router.delete('/:userId/:taskId', middleware.validateToken, function(req, res, next){
  var task = req.user.list.id(req.params.taskId);
  task.remove((err) => {
    if (err){
      return res.status(403).send({
        success: false,
        message: err.message
      });
    }
  });
  req.user.save((err) => {
    if (err) {
      return res.status(403).send({
        success: false,
        message: err.message
      });
    }
    return res.status(200).send({
      success: true,
      data: null
    });
  });
  // req.user.update({_id: req.params.userId}).pull({list: {_id: req.params.taskId}});
});

module.exports = router;
